OpenGL-Playpen
==============

An experimental Android app to learn and understand OpenGL in the form of a git-based log
in which the commits hold the progression I took to get things working.

You will need to supply your own images which will be used as the textures in the playpen. They are referenced by the class
KosmosObjectRenderer's onSurfaceCreated() method where they are stored in the sources array.

*NOTE* Images might need to have dimensions which are each an integral power of two (16, 32, 64, 128, 256, etc) to display
correctly due to the GPU on the rendering device (such as the Kindle Fire).

Current Features
================

A polygon is displayed, either a cube or pyramid and selectable by a menu item, is displayed.

There are four ways to control the interactions: changing an object's orientation or its location
within the space, and changing the observer's orientation or location within the space.

There is a class to render fog using one of several techniques (taken from Chris Aliotta's code
in Lesson 16 on NeHe), selectable by a menu item.

There is a "Display Axes" menu item to toggle the display of the axes of the space.

There is a debug menu item to set/reset a global variable which can be tested when rendering. It's
referenced in some places to draw a wireframe to debug vector or texel arrays.
