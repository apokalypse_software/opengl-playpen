/*
 * (C) Copyright 2012 Apokalypse Software Corp.
 * 
 * Portions Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apokalypsesoftware.kosmic.builder;

import static android.opengl.GLES10.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * A vertex shaded pyramid.
 */
class Pyramid implements KosmosObject
{
	private IntBuffer   mVertexBuffer;
	private FloatBuffer mTexBuffer;
	private ByteBuffer  mIndexBuffer;

	public Pyramid()
	{
		int one = 0x10000;

		/*
		 * The initial vertex definition
		 *
		 * Note that each face is defined, even
		 * if indices are available, because
		 * of the texturing we want to achieve 
		 */ 
		final int vertices2[] = {
			//Vertices according to faces
			   0,  one,    0,	//Vertex 0
			-one, -one,  one,	//v1
			 one, -one,  one,	//v2

			   0,  one,    0,	// Right
			 one, -one,  one,
			 one, -one, -one,

			   0,  one,    0,	// Back
			 one, -one, -one,
			-one, -one, -one,

			   0,  one,    0,	// Left
			-one, -one, -one,
			-one, -one,  one,

			-one, -one,  one,	// Base
			-one, -one, -one,
			 one, -one, -one,
			 one, -one,  one,
		};

		/** The initial texture coordinates (u, v) */   
		final float texture2[] = { 
			// Mapping texture coordinates for the vertices
			// The pyramid's vertices are defined in CCW order
			0.5f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,

			0.5f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,

			0.5f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,

			0.5f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,

			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f
		};

		/** The initial indices definition */   
		final byte indices2[] = {
			//Faces definition
			 0,  1,  3,		0,   3,  2,		//Face front
			 4,  5,  7,		4,   7,  6,		//Face right
			 8,  9, 11,		8,  11, 10,
			12, 13, 15,		12, 15, 14,
			16, 17, 19,		16, 19, 18,
			20, 21, 23,		20, 23, 22,
		};

		/*
		 *	Buffers to be passed to gl*Pointer() functions
		 *	must be direct, i.e., they must be placed on the
		 *	native heap where the garbage collector cannot
		 *	move them.
		 *
		 *	Buffers with multi-byte datatypes (e.g., short, int, float)
		 *	must have their byte order set to native order
		 */
		ByteBuffer vbb = ByteBuffer.allocateDirect(vertices2.length*4);
		vbb.order(ByteOrder.nativeOrder());
		mVertexBuffer = vbb.asIntBuffer();
		mVertexBuffer.put(vertices2);
		mVertexBuffer.position(0);

		ByteBuffer tbb = ByteBuffer.allocateDirect(texture2.length * 2 * 4);
		tbb.order(ByteOrder.nativeOrder());
		mTexBuffer = tbb.asFloatBuffer();

		mTexBuffer.put(texture2);

		mTexBuffer.position(0);

		mIndexBuffer = ByteBuffer.allocateDirect(indices2.length);
		mIndexBuffer.put(indices2);
		mIndexBuffer.position(0);
	}

	public void draw(GL10 aGLContext, int aTextureIDs[])
	{
		glFrontFace(GL_CCW);
		glVertexPointer(3, GL_FIXED, 0, mVertexBuffer);

		glEnable(GL_TEXTURE_2D);
		glTexCoordPointer(2, GL_FLOAT, 0, mTexBuffer);

		glColor4f(1, 1, 1, 1);
		for(int i = 0; i < 4; ++i)
		{
			glBindTexture(GL_TEXTURE_2D, aTextureIDs[i]);   //use texture of ith face
			mIndexBuffer.position(3 * i);  //select ith face

			//draw triangle making up this face
			// glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_BYTE, mIndexBuffer);
			glDrawArrays(GL_TRIANGLES, i * 3, 3);
		}
		glBindTexture(GL_TEXTURE_2D, aTextureIDs[5]);   //use 5th texture for base
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	}
}
