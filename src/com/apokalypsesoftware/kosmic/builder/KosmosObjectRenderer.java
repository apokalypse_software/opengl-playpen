/*
 * (C) Copyright 2012 Apokalypse Software Corp.
 * 
 * Portions Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apokalypsesoftware.kosmic.builder;

import static android.opengl.GLES10.*;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;

/*
 * Render a Kosmos object.
 */
class KosmosObjectRenderer implements GLSurfaceView.Renderer
{
	private KosmosObject				mObject;
	public	ObjectOrientation			mOrientation = new ObjectOrientation();
	public	Point3D						mLocation = new Point3D();
	public	float						mHeading;
	public	Point3D						mObserverLocation = new Point3D(0, 0, -2);
	public	ObserverAttitude			mObserverAttitude = new ObserverAttitude();
	private	int							textures[] = new int[6];
	private	KosmosMatricesCoordinator	mMatricesCoordinator;
	private	float[]						kosmosMatrix		= new float[16];
	private	Context						mContext;
	private KosmosFogBank				mFogBank;
	private	KosmicAxes					mAxes;

	private	boolean						mDisplayAxes;

	public KosmosObjectRenderer(Context aContext)
	{

		mContext = aContext;
		mFogBank = new KosmosFogBank();
		mAxes = new KosmicAxes();
		mMatricesCoordinator = new KosmosMatricesCoordinator();
		updateObserverMatrix();
	}

	public boolean isDisplayAxes()
	{
		return	(mDisplayAxes);
	}

	public void setDisplayAxes(boolean aDisplayAxes)
	{

		if(mDisplayAxes != aDisplayAxes)
			mDisplayAxes = aDisplayAxes;
	}

	public KosmosObject getKosmosObject()
	{
		return	(mObject);
	}

	public void setKosmosObject(KosmosObject anObject)
	{
		mObject = anObject;
	}

	public Point3D getObjectLocation()
	{
		return	(mLocation);
	}

	public void setObjectLocation(Point3D aLocation)
	{

		aLocation.x = Math.min(aLocation.x, 3);
		aLocation.x = Math.max(aLocation.x, -5.5f);
		aLocation.y = Math.min(aLocation.y, 3);
		aLocation.y = Math.max(aLocation.y, -5.5f);
		aLocation.z = Math.min(aLocation.z, 2.5f);
		aLocation.z = Math.max(aLocation.z, -6);
		if(aLocation.equals(mLocation) != true)
			mLocation = aLocation;
	}

	public ObjectOrientation getObjectOrientation()
	{
		return	(mOrientation);
	}

	public void setObjectOrientation(ObjectOrientation anOrientation)
	{

		if(mOrientation.equals(anOrientation) != true)
			mOrientation = anOrientation;
	}

	public ObserverAttitude getObserverAttitude()
	{
		return	(mObserverAttitude);
	}

	public void setObserverAttitude(ObserverAttitude anAttitude)
	{

		if(mObserverLocation.equals(anAttitude) != true)
		{
			mObserverAttitude = anAttitude;
			updateObserverMatrix();
		}
	}

	public Point3D getObserverLocation()
	{
		return	(mObserverLocation);
	}

	public void setObserverLocation(Point3D aLocation)
	{

		aLocation.x = Math.min(aLocation.x, 3);
		aLocation.x = Math.max(aLocation.x, -5.5f);
		aLocation.y = Math.min(aLocation.y, 3);
		aLocation.y = Math.max(aLocation.y, -5.5f);
		aLocation.z = Math.min(aLocation.z, 2.5f);
		aLocation.z = Math.max(aLocation.z, -6);
		if(mObserverLocation.equals(aLocation) != true)
		{
			mObserverLocation = aLocation;
			updateObserverMatrix();
		}
	}

	KosmosFogBank getFogBank()
	{
		return	(mFogBank);
	}

	void setFogBank(KosmosFogBank aFogBank)
	{
		mFogBank = aFogBank;
	}

	public void onDrawFrame(GL10 gl) {
		float	mvpMatrix[];

		/*
		 * By default, OpenGL enables features that improve quality
		 * but reduce performance. One might want to tweak that
		 * especially on software renderer.
		 */
		glDisable(GL_DITHER);
		glTexEnvx(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		/*
		 * Usually, the first thing one might want to do is to clear
		 * the screen. The most efficient way of doing this is to use
		 * glClear().
		 */
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/*
		 * Now we're ready to draw some 3D objects
		 */
		glMatrixMode(GL_MODELVIEW);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textures[0]);
		glTexParameterx(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterx(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		mvpMatrix = mMatricesCoordinator.getFinalMatrix();

		mMatricesCoordinator.gatherTransformsFromKosmos(this);
		System.arraycopy(mvpMatrix, 0, kosmosMatrix, 0, 16);
		glLoadMatrixf(kosmosMatrix, 0);

		// Render the object
		glPushMatrix();
		mMatricesCoordinator.gatherTransformsFromKosmosObject(this, mObject);
		glLoadMatrixf(mvpMatrix, 0);
		mObject.draw(gl, textures);
		glPopMatrix();

		// Render Kosmos environment and fixtures
		mFogBank.draw();
		if(mDisplayAxes != false)
			mAxes.draw(gl, textures);
	}

	public void onSurfaceChanged(GL10 gl, int width, int height) {
		glViewport(0, 0, width, height);

		glMatrixMode(GL_PROJECTION);

		/*
		 * Set our projection matrix. This doesn't have to be done
		 * each time we draw, but usually a new projection needs to
		 * be set when the viewport is resized.
		 */
		mMatricesCoordinator.setProjectionMatrixWithDefaultForWidthHeight(width, height);
		glLoadMatrixf(mMatricesCoordinator.getProjectionMatrix(), 0);
	}

	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		Bitmap	bitmap;

		/*
		 * By default, OpenGL enables features that improve quality
		 * but reduce performance. One might want to tweak that
		 * especially on software renderer.
		 */
		glDisable(GL_DITHER);

		/*
		 * Some one-time OpenGL initialization can be made here
		 * probably based on features of this particular context
		 */
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);

		glClearColor(0, 0, 0, 1);

		glEnable(GL_CULL_FACE);
		glShadeModel(GL_SMOOTH);
		glEnable(GL_DEPTH_TEST);

		glEnable(GL_TEXTURE_2D);

		/*
		 * Create our texture. This has to be done each time the
		 * surface is created.
		 */

		glGenTextures(6, textures, 0);

		/*
		 * TODO: Replace the entries with the references to your own images
		 * 		 which will be the textures for the shapes.
		 *
		 * NOTE: The GPU on some devices (such as the Kindle Fire) require the
		 * 		 images to have dimensions which are each a power-of-two to
		 * 		 display properly, or the face may appear white.
		 */
		int sources[] = {
			R.drawable.dphclub_com_12434413781_1,
			R.drawable.dphclub_com_12434414341_6,
			R.drawable.dphclub_com_12434414621_12,
			R.drawable.dphclub_com_12434414691_8,
			R.drawable.dphclub_com_12434414721,
			R.drawable.dphclub_com_12434414801_11
		};

		for(int i = 0; i < 6; ++i)
		{
			glBindTexture(GL_TEXTURE_2D, textures[i]);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

			InputStream is = mContext.getResources().openRawResource(sources[i]);
			try {
				bitmap = BitmapFactory.decodeStream(is);
			} finally {
				try {
					is.close();
				} catch(IOException e) {
					// Ignore.
				}
			}

			GLUtils.texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);
			bitmap.recycle();
		}
	}

	public void updateObserverMatrix()
	{
		mMatricesCoordinator.coordinateMatricesForObserver(this);
	}
}
