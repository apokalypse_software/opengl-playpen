/*
 * (C) Copyright 2012 Apokalypse Software Corp.
 *
 * Portions taken from http://www.learnopengles.com/rotating-an-object-with-touch-events/
 * 	and Copyright 2011-2012 Learn OpenGL ES.
 *
 * Portions Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.apokalypsesoftware.kosmic.builder;

import android.opengl.Matrix;

class	KosmosMatricesCoordinator
{
	KosmosObjectRenderer	mKosmos;
	private	float[]			mIdentityMatrix = new float[16];

	/** Store the accumulated rotation. */
	private final float[]	mAccumulatedRotation = new float[16];

	/** Store the current rotation. */
	private	final float[]	mCurrentRotation = new float[16];
	private	float[]			mModelMatrix = new float[16];
	private	float[]			mTemporaryMatrix = new float[16];
	private float[]			mObserverMatrix = new float[16];
	private float[]			mViewMatrix = new float[16];
	private float[]			mMVPMatrix = new float[16];
	private float[]			mProjectionMatrix = new float[16];

	public KosmosMatricesCoordinator()
	{

		Matrix.setIdentityM(mIdentityMatrix, 0);

		// Position the eye behind the origin.
		final float		eyeX = 0.0f;
		final float		eyeY = 0.0f;
		final float		eyeZ = 1.5f;

		// We are looking toward the distance
		final float		lookX = 0.0f;
		final float		lookY = 0.0f;
		final float		lookZ = -5.0f;

		// Set our up vector. This is where our head would be pointing were we holding the camera.
		final float		upX = 0.0f;
		final float		upY = 1.0f;
		final float		upZ = 0.0f;

		/*
		 *	Set the view matrix. This matrix can be said to represent the camera position.
		 *
		 *	NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
		 *	view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
		 *	Android 2.1 (aka Froyo, SDK 7) is OpenGL ES 1, so we pulled in setLookAtM() source.
		 */
		setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);

		// Initialize the accumulated rotation matrix
		Matrix.setIdentityM(mAccumulatedRotation, 0);
	}

	public float[] getFinalMatrix()
	{
		return	(mMVPMatrix);
	}

	public float[] getProjectionMatrix()
	{
		return	(mProjectionMatrix);
	}

	public void setProjectionMatrixWithDefaultForWidthHeight(int aWidth, int aHeight)
	{

		// Create a new perspective projection matrix. The height will stay the same
		// while the width will vary as per aspect ratio.
		final float ratio = (float)aWidth / aHeight;
		final float left = -ratio;
		final float right = ratio;
		final float bottom = -1.0f;
		final float top = 1.0f;
		final float near = 1.0f;
		final float far = 10.0f;

		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
	}

	public void gatherTransformsFromKosmosObject(KosmosObjectRenderer aKosmos, KosmosObject anObject)
	{

		mKosmos = aKosmos;
		computeDeltaMatrixForModel();
		computeNetMatrixForModel();
		integrateNetMatrixIntoModel();

		integrateObjectIntoMVP(mModelMatrix);
	}

	public void gatherTransformsFromKosmos(KosmosObjectRenderer aKosmos)
	{

		mKosmos = aKosmos;
		integrateObjectIntoMVP(mIdentityMatrix);
	}

	private void computeDeltaMatrixForModel()
	{

		// Set a matrix that contains the current rotation.
		Matrix.setIdentityM(mCurrentRotation, 0);
		Matrix.rotateM(mCurrentRotation, 0, mKosmos.mOrientation.angleX, 0.0f, 1.0f, 0.0f);
		Matrix.rotateM(mCurrentRotation, 0, mKosmos.mOrientation.angleY, 1.0f, 0.0f, 0.0f);
		mKosmos.mOrientation.angleX = 0.0f;
		mKosmos.mOrientation.angleY = 0.0f;
	}

	private void computeNetMatrixForModel()
	{

		// Multiply the current rotation by the accumulated rotation, and then set the accumulated
		// rotation to the result.
		Matrix.multiplyMM(mTemporaryMatrix, 0, mCurrentRotation, 0, mAccumulatedRotation, 0);
		System.arraycopy(mTemporaryMatrix, 0, mAccumulatedRotation, 0, 16);
	}

	private void integrateNetMatrixIntoModel()
	{

		// Draw a cube.
		// Translate the cube into the screen.
		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, mKosmos.mLocation.x, mKosmos.mLocation.y, mKosmos.mLocation.z);

		// Rotate the cube taking the overall rotation into account.
		Matrix.multiplyMM(mTemporaryMatrix, 0, mModelMatrix, 0, mAccumulatedRotation, 0);
		System.arraycopy(mTemporaryMatrix, 0, mModelMatrix, 0, 16);
	}

	public void coordinateMatricesForObserver(KosmosObjectRenderer aKosmos)
	{

		System.arraycopy(mViewMatrix, 0, mObserverMatrix, 0, 16);
		Matrix.rotateM(mObserverMatrix, 0, aKosmos.mObserverAttitude.pitch, 1, 0, 0);
		Matrix.rotateM(mObserverMatrix, 0, aKosmos.mObserverAttitude.yaw, 0, 1, 0);
		Matrix.translateM(mObserverMatrix, 0, aKosmos.mObserverLocation.x, aKosmos.mObserverLocation.y, aKosmos.mObserverLocation.z);
	}

	private void integrateObjectIntoMVP(float[] anObjectMatrix)
	{

		// This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
		// (which currently contains model * view).
		Matrix.multiplyMM(mMVPMatrix, 0, mObserverMatrix, 0, anObjectMatrix, 0);
	}

	void setLookAtM(float[] rm, int rmOffset, float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ)
	{
		// See the OpenGL GLUT documentation for gluLookAt for a description
		// of the algorithm. We implement it in a straightforward way:
		float fx = centerX - eyeX;
		float fy = centerY - eyeY;
		float fz = centerZ - eyeZ;

		// Normalize f
		float rlf = 1.0f / Matrix.length(fx, fy, fz);
		fx *= rlf;
		fy *= rlf;
		fz *= rlf;

		// compute s = f x up (x means "cross product")
		float sx = fy * upZ - fz * upY;
		float sy = fz * upX - fx * upZ;
		float sz = fx * upY - fy * upX;

		// and normalize s
		float rls = 1.0f / Matrix.length(sx, sy, sz);
		sx *= rls;
		sy *= rls;
		sz *= rls;

		// compute u = s x f
		float ux = sy * fz - sz * fy;
		float uy = sz * fx - sx * fz;
		float uz = sx * fy - sy * fx;
		rm[rmOffset + 0] = sx;
		rm[rmOffset + 1] = ux;
		rm[rmOffset + 2] = -fx;
		rm[rmOffset + 3] = 0.0f;
		rm[rmOffset + 4] = sy;
		rm[rmOffset + 5] = uy;
		rm[rmOffset + 6] = -fy;
		rm[rmOffset + 7] = 0.0f;
		rm[rmOffset + 8] = sz;
		rm[rmOffset + 9] = uz;
		rm[rmOffset + 10] = -fz;
		rm[rmOffset + 11] = 0.0f;
		rm[rmOffset + 12] = 0.0f;
		rm[rmOffset + 13] = 0.0f;
		rm[rmOffset + 14] = 0.0f;
		rm[rmOffset + 15] = 1.0f;
		Matrix.translateM(rm, rmOffset, -eyeX, -eyeY, -eyeZ);
	}
}
