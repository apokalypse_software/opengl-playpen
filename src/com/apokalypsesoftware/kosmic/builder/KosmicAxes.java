/*
 * (C) Copyright 2012 Apokalypse Software Corp.
 * 
 * Portions Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apokalypsesoftware.kosmic.builder;

import static android.opengl.GLES10.*;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

/*
 * An object within Kosmos rendered to show the axes within Kosmos.
 */
public class KosmicAxes implements KosmosObject
{
	private	FloatBuffer	mVertexBuffer;
	private	ByteBuffer	mIndexBuffer;
	private	int			mGridColors[][] = {
		{ 1, 0, 0, 1 },
		{ 0, 1, 0, 1 },
		{ 0, 0, 1, 1 }
	};

	public KosmicAxes()
	{
		float	one = 2;
		int		zero = 0;
		float	fourth = 0.25f;

		/*
		 * The initial vertex definition
		 */ 
		final float vertices[] = {
			-one,             zero, zero,	// X axis
			 one,             zero, zero,
			-one + fourth,  fourth, zero,
			-one + fourth, -fourth, zero,
			 one - fourth,  fourth, zero,
			 one - fourth, -fourth, zero,

			 zero,            -one, zero,	// Y axis
			 zero,             one, zero,
			 fourth, -one + fourth, zero,
			-fourth, -one + fourth, zero,
			 fourth,  one - fourth, zero,
			-fourth,  one - fourth, zero,

			zero,    zero,          -one,	// Z axis
			zero,    zero,           one,
			zero,  fourth, -one + fourth, 
			zero, -fourth, -one + fourth, 
			zero,  fourth,  one - fourth, 
			zero, -fourth,  one - fourth, 
		};

		/* The indices for the axes visual elements	*/
		final byte indices[] = {
			 0,  1,						// Endpoints for X axis
			 0,  2,  0,  3,				// Left arrowhead
			 1,  4,  1,  5,				// Right arrowhead
			 6,  7,						// Endpoints for Y axis
			 6,  8,  6,  9,				// Bottom arrowhead
			 7, 10,  7, 11,				// Top arrowhead
			12, 13,						// Endpoints for Z axis
			12, 14, 12, 15,				// Back arrowhead
			13, 16, 13, 17				// Front arrowhead
		};

		/*
		 *	Buffers to be passed to gl*Pointer() functions
		 *	must be direct, i.e., they must be placed on the
		 *	native heap where the garbage collector cannot
		 *	move them.
		 *
		 *	Buffers with multi-byte datatypes (e.g., short, int, float)
		 *	must have their byte order set to native order
		 */
		ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
		vbb.order(ByteOrder.nativeOrder());
		mVertexBuffer = vbb.asFloatBuffer();
		mVertexBuffer.put(vertices);
		mVertexBuffer.position(0);

		mIndexBuffer = ByteBuffer.allocateDirect(indices.length);
		mIndexBuffer.put(indices);
		mIndexBuffer.position(0);
	}

	public void draw(GL10 gl, int textures[])
	{
		glDisable(GL_TEXTURE_2D);
		glEnable(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, 0, mVertexBuffer);

		for(int i = 0; i < 3; ++i)
		{
			glColor4f(mGridColors[i][0], mGridColors[i][1], mGridColors[i][2], mGridColors[i][3]);

			mIndexBuffer.position(10 * i);			// Select ith axis
			glDrawElements(GL_LINES, 10, GL_UNSIGNED_BYTE, mIndexBuffer);
		}
	}
}
