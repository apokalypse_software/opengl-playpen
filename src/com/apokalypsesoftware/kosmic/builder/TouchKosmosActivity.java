/*
 * (C) Copyright 2012 Apokalypse Software Corp.
 * 
 * Portions Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apokalypsesoftware.kosmic.builder;

import com.apokalypsesoftware.kosmic.builder.TouchSurfaceView.KosmosObjectShape;
import com.apokalypsesoftware.kosmic.builder.TouchSurfaceView.UserInterfaceMode;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;

/**
 * Wrapper activity demonstrating the use of {@link GLSurfaceView}, a view
 * that uses OpenGL drawing into a dedicated surface.
 *
 * Shows:
 * + How to redraw in response to user input.
 */
public class TouchKosmosActivity extends Activity {
	private TouchSurfaceView mGLSurfaceView;
	private int					mFogMode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Create our Preview view and set it as the content of our
		// Activity
		mGLSurfaceView = new TouchSurfaceView(this);
		mGLSurfaceView.setShape(KosmosObjectShape.Cube);
		setContentView(mGLSurfaceView);
		mGLSurfaceView.requestFocus();
		mGLSurfaceView.setFocusableInTouchMode(true);
	}

	@Override
	protected void onResume() {
		// Ideally a game should implement onResume() and onPause()
		// to take appropriate action when the activity looses focus
		super.onResume();
		mGLSurfaceView.onResume();
	}

	@Override
	protected void onPause() {
		// Ideally a game should implement onResume() and onPause()
		// to take appropriate action when the activity looses focus
		super.onPause();
		mGLSurfaceView.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate menu from XML resource
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.touch_kosmos_options_menu, menu);

		// Generate any additional actions that can be performed on the
		// overall list.  In a normal install, there are no additional
		// actions found here, but this allows other applications to extend
		// our menu with their own actions.
		Intent intent = new Intent(null, getIntent().getData());
		intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
		menu.addIntentOptions(Menu.CATEGORY_ALTERNATIVE, 0, 0, new ComponentName(this, TouchKosmosActivity.class), null, intent, 0, null);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu aMenu)
	{
		// menu.setGroupVisible(R.id.menu_group_edit, true);

		MenuAssistant.setCheckOnNthItemInGroupIDInMenu(mGLSurfaceView.getShape().ordinal(),             R.id.menu_shape,   aMenu);
		MenuAssistant.setCheckOnNthItemInGroupIDInMenu(mGLSurfaceView.getUserInterfaceMode().ordinal(), R.id.menu_control, aMenu);
		MenuAssistant.setCheckOnNthItemInGroupIDInMenu(mFogMode,     R.id.menu_fog,     aMenu);
		return	(super.onPrepareOptionsMenu(aMenu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		boolean			optionConsumed;
		int				itemId;
		int				classNameIndex;
		int				fogModeIndex;
		int				controlIndex;
		KosmosObjectShape[]	shapeLookup;
		UserInterfaceMode[]	controlLookup;

		optionConsumed = false;
		itemId = item.getItemId();
		switch(itemId)
		{
		case	R.id.menu_shape_cube:
		case	R.id.menu_shape_pyramid:
				classNameIndex = itemId - R.id.menu_shape_cube;
				shapeLookup = KosmosObjectShape.values();
				if((classNameIndex >= 0) && (classNameIndex < shapeLookup.length))
					mGLSurfaceView.setShape(shapeLookup[classNameIndex]);
				optionConsumed = true;
				break;

		case	R.id.menu_control_object_orientation:
		case	R.id.menu_control_object_position:
		case	R.id.menu_control_view_orientation:
		case	R.id.menu_control_view_position:
				controlIndex = itemId - R.id.menu_control_object_orientation;
				controlLookup = UserInterfaceMode.values();
				if((controlIndex >= 0) && (controlIndex < controlLookup.length))
					mGLSurfaceView.setUserInterfaceMode(controlLookup[controlIndex]);
				optionConsumed = true;
				break;

		case	R.id.menu_fog_none:
		case	R.id.menu_fog_exp:
		case	R.id.menu_fog_exp2:
		case	R.id.menu_fog_linear:
				fogModeIndex = itemId - R.id.menu_fog_none;
				if((fogModeIndex >= 0) && (fogModeIndex < 4))
				{
					mFogMode = fogModeIndex;
					mGLSurfaceView.getKosmosRenderer().getFogBank().setMode(fogModeIndex);
					mGLSurfaceView.requestRender();
				}
				optionConsumed = true;
				break;

		case	R.id.menu_axes:
				mGLSurfaceView.setDisplayAxes(mGLSurfaceView.isDisplayAxes() != true);
				optionConsumed = true;
				break;

		case	R.id.menu_about:
				showAbout();
				optionConsumed = true;
				break;

		default:
				optionConsumed = super.onOptionsItemSelected(item);
		}
		return	(optionConsumed);
	}

	private void showAbout()
	{

		//set up dialog
		Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.kosmic_builder_about);
		dialog.setTitle("Kosmic Builder");
		dialog.setCancelable(true);
		dialog.show();
	}
}

/**
 * Implement a simple rotation control.
 *
 */
class TouchSurfaceView extends GLSurfaceView {

	enum	KosmosObjectShape	{
		Cube,
		Pyramid
	}

	enum UserInterfaceMode	{
		ModifyObjectOrientation,
		ModifyObjectLocation,
		ModifyObserverOrientation,
		ModifyObserverLocation
	}

	final static float				TRACKBALL_SCALE_FACTOR = 36.0f;
	private KosmosObjectRenderer	mRenderer;
	float							mDensity;
	private EventHandler			mEventHandler;
	private	KosmosObjectShape		mObjectShape;
	private	UserInterfaceMode		mUserInterfaceMode;

	public TouchSurfaceView(Context context) {
		super(context);
		mRenderer = new KosmosObjectRenderer(context);
		setRenderer(mRenderer);
		setShape(KosmosObjectShape.Cube);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		final DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		mDensity = displayMetrics.density;

		setUserInterfaceMode(UserInterfaceMode.ModifyObjectOrientation);
		mEventHandler.setClient(this);
		mEventHandler.setSubject(mRenderer);
	}

	public boolean isDisplayAxes()
	{
		return	(mRenderer.isDisplayAxes());
	}

	public void setDisplayAxes(boolean aDisplayAxes)
	{

		if(mRenderer.isDisplayAxes() != aDisplayAxes)
		{
			mRenderer.setDisplayAxes(aDisplayAxes);
			requestRender();
		}
	}

	public KosmosObjectShape getShape()
	{
		return	(mObjectShape);
	}

	public void setShape(KosmosObjectShape aShape)
	{
		String			className;
		KosmosObject	kosmosObject;
		final String	shapeClassNames[] = {
		"com.apokalypsesoftware.kosmic.builder.Cube",
		"com.apokalypsesoftware.kosmic.builder.Pyramid"
		};

		mObjectShape = aShape;
		className = shapeClassNames[mObjectShape.ordinal()];
		try
		{
			kosmosObject = (KosmosObject)Class.forName(className).newInstance();
			mRenderer.setKosmosObject(kosmosObject);
			requestRender();
		}
		catch(Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	KosmosObjectRenderer getKosmosRenderer()
	{
		return	(mRenderer);
	}

	public void setKosmosRenderer(KosmosObjectRenderer aRenderer)
	{
		mRenderer = aRenderer;
	}

	public UserInterfaceMode getUserInterfaceMode()
	{
		return	(mUserInterfaceMode);
	}

	@SuppressWarnings("unchecked")
	public void setUserInterfaceMode(UserInterfaceMode aUIMode)
	{
		final String	controlClassNames[] = {
			"com.apokalypsesoftware.kosmic.builder.ModifyObjectOrientationEventHandler",
			"com.apokalypsesoftware.kosmic.builder.ModifyObjectLocationEventHandler",
			"com.apokalypsesoftware.kosmic.builder.ModifyObserverOrientationEventHandler",
			"com.apokalypsesoftware.kosmic.builder.ModifyObserverLocationEventHandler"
		};
		String						className;
		Class<EventHandler>			eventHandlerClass;
		EventHandler				eventHandler;

		mUserInterfaceMode = aUIMode;
		className = controlClassNames[aUIMode.ordinal()];
		try
		{
			eventHandlerClass = (Class<EventHandler>)Class.forName(className);
			if(eventHandlerClass != null)
			{
				eventHandler = eventHandlerClass.newInstance();
				if(eventHandler != null)
				{
					eventHandler.setClient(this);
					eventHandler.setSubject(getKosmosRenderer());
					setEventHandler(eventHandler);
				}
			}
		}
		catch(Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private EventHandler getEventHandler()
	{
		return	(mEventHandler);
	}

	private void setEventHandler(EventHandler anEventHandler)
	{
		mEventHandler = anEventHandler;
	}

	@Override public boolean onTrackballEvent(MotionEvent e) {
		boolean	eventResult;

		eventResult = mEventHandler.onTrackballEvent(e);
		if(eventResult != true)
			eventResult = super.onTouchEvent(e);
		return	(eventResult);
	}

	@Override public boolean onTouchEvent(MotionEvent e) {
		boolean	eventResult;

		eventResult = mEventHandler.onTouchEvent(e);
		if(eventResult != true)
			eventResult = super.onTouchEvent(e);
		return	(eventResult);
	}

	public Point3D getObjectLocation()
	{
		return	(mRenderer.getObjectLocation());
	}

	public void setObjectLocation(KosmosObjectRenderer aSubject, Point3D aLocation)
	{

		mRenderer.setObjectLocation(aLocation);
		requestRender();
	}

	public ObjectOrientation getObjectOrientation(KosmosObjectRenderer aSubject)
	{
		return	(mRenderer.getObjectOrientation());
	}

	public void setObjectOrientation(KosmosObjectRenderer aSubject, ObjectOrientation anOrientation)
	{

		mRenderer.setObjectOrientation(anOrientation);
		requestRender();
	}

	public Point3D getObserverLocation()
	{
		return	(mRenderer.getObserverLocation());
	}

	public void setObserverLocation(KosmosObjectRenderer aSubject, Point3D aLocation)
	{
		mRenderer.setObserverLocation(aLocation);
		requestRender();
	}

	public ObserverAttitude getObserverAttitude(KosmosObjectRenderer aSubject)
	{
		return	(mRenderer.getObserverAttitude());
	}

	public void setObserverAttitude(KosmosObjectRenderer aSubject, ObserverAttitude anAttitude)
	{

		mRenderer.setObserverAttitude(anAttitude);
		requestRender();
	}
}

