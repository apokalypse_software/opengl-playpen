/*
 * (C) Copyright 2012 Apokalypse Software Corp.
 * 
 * Portions Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apokalypsesoftware.kosmic.builder;

import android.view.MotionEvent;

class	Point3D
{
	float			x;
	float			y;
	float			z;

	public Point3D()
	{
		x = y = z = 0;
	}

	public Point3D(Point3D aPoint)
	{

		x = aPoint.x;
		y = aPoint.y;
		z = aPoint.z;
	}

	public Point3D(float anX, float aY, float aZ)
	{

		x = anX;
		y = aY;
		z = aZ;
	}

	public boolean equals(Object anObject)
	{

		return	((anObject instanceof Point3D)
				&& (((Point3D)anObject).x == x)
				&& (((Point3D)anObject).y == y)
				&& (((Point3D)anObject).z == z));
	}

	public int hashCode()
	{
		return	((int)(x * y * z));
	}
}

class	ObjectOrientation
{
	float			angleX;
	float			angleY;
	float			angleZ;

	public ObjectOrientation()
	{
		angleX = angleY = angleZ = 0;
	}

	public ObjectOrientation(ObjectOrientation anAttitude)
	{

		angleX = anAttitude.angleX;
		angleY = anAttitude.angleY;
		angleZ = anAttitude.angleZ;
	}

	public boolean equals(Object anObject)
	{

		return	((anObject instanceof Point3D)
				&& (((ObjectOrientation)anObject).angleX == angleX)
				&& (((ObjectOrientation)anObject).angleY == angleY)
				&& (((ObjectOrientation)anObject).angleZ == angleZ));
	}

	public int hashCode()
	{
		return	((int)(angleX * angleY * angleZ));
	}
}

class	ObserverAttitude
{
	float			roll;
	float			pitch;
	float			yaw;

	public ObserverAttitude()
	{
		roll = pitch = yaw = 0;
	}

	public ObserverAttitude(ObserverAttitude anAttitude)
	{

		roll = anAttitude.roll;
		pitch = anAttitude.pitch;
		yaw = anAttitude.yaw;
	}

	public boolean equals(Object anObject)
	{

		return	((anObject instanceof Point3D)
				&& (((ObserverAttitude)anObject).roll == roll)
				&& (((ObserverAttitude)anObject).pitch == pitch)
				&& (((ObserverAttitude)anObject).yaw == yaw));
	}

	public int hashCode()
	{
		return	((int)(roll * pitch * yaw));
	}
}

public interface EventHandler
{
	public TouchSurfaceView getClient();
	public void setClient(TouchSurfaceView aClient);
	public KosmosObjectRenderer getSubject();
	public void setSubject(KosmosObjectRenderer aSubject);

	public boolean onTouchEvent(MotionEvent e);
	public boolean onTrackballEvent(MotionEvent e);
}

class KosmosEventHandler implements EventHandler
{
	TouchSurfaceView		mClient;
	KosmosObjectRenderer	mSubject;
	private float			mPreviousX;
	private float			mPreviousY;

	public TouchSurfaceView getClient()
	{
		return	(mClient);
	}

	public void setClient(TouchSurfaceView aClient)
	{
		mClient = aClient;
	}

	public KosmosObjectRenderer getSubject()
	{
		return	(mSubject);
	}

	public void setSubject(KosmosObjectRenderer aSubject)
	{
		mSubject = aSubject;
	}

	public boolean onTouchEvent(MotionEvent e)
	{
		boolean	eventResult;

		eventResult = false;
		if(e != null)
		{
			float x = e.getX();
			float y = e.getY();

			switch(e.getAction())
			{
			case	MotionEvent.ACTION_MOVE:
					float dx = (x - mPreviousX) / mClient.mDensity / 2f;
					float dy = (y - mPreviousY) / mClient.mDensity / 2f;
					handleMove(dx, dy);
			}
			mPreviousX = x;
			mPreviousY = y;
			eventResult = true;
		}
		return	(eventResult);
	}

	@Override
	public boolean onTrackballEvent(MotionEvent e)
	{
		boolean	eventResult;

		eventResult = false;
		if(e != null)
		{
			float x = e.getX();
			float y = e.getY();

			float dx = x * TouchSurfaceView.TRACKBALL_SCALE_FACTOR;
			float dy = y * TouchSurfaceView.TRACKBALL_SCALE_FACTOR;

			handleMove(dx, dy);
			eventResult = true;
		}
		return	(eventResult);
	}

	public void handleMove(float aDeltaX, float aDeltaY)
	{
	}
}

class ModifyObjectLocationEventHandler extends KosmosEventHandler
{

	public void handleMove(float aDeltaX, float aDeltaY)
	{

		if(mSubject != null)
		{
			Point3D		location;

			location = new Point3D(mClient.getObjectLocation());
			location.x += aDeltaX;
			location.z += aDeltaY;
			mClient.setObjectLocation(mSubject, location);
		}
	}
}

class ModifyObjectOrientationEventHandler extends KosmosEventHandler
{

	public void handleMove(float aDeltaX, float aDeltaY)
	{

		if(mSubject != null)
		{
			ObjectOrientation	orientation;

			orientation = new ObjectOrientation(mClient.getObjectOrientation(mSubject));
			orientation.angleX += aDeltaX;
			orientation.angleY += aDeltaY;
			mClient.setObjectOrientation(mSubject, orientation);
		}
	}
}

class ModifyObserverLocationEventHandler extends KosmosEventHandler
{

	public void handleMove(float aDeltaX, float aDeltaY)
	{

		if(mSubject != null)
		{
			Point3D		location;

			location = new Point3D(mClient.getObserverLocation());
			location.x += aDeltaX;
			location.z += aDeltaY;
			mClient.setObserverLocation(mSubject, location);
		}
	}
}

class ModifyObserverOrientationEventHandler extends KosmosEventHandler
{

	public void handleMove(float aDeltaX, float aDeltaY)
	{

		if(mSubject != null)
		{
			ObserverAttitude	attitude;

			attitude = new ObserverAttitude(mClient.getObserverAttitude(mSubject));
			attitude.yaw -= aDeltaX;
			attitude.pitch -= aDeltaY;
			mClient.setObserverAttitude(mSubject, attitude);
		}
	}
}
