/*
 * (C) Copyright 2012 Apokalypse Software Corp.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apokalypsesoftware.kosmic.builder;

import android.view.Menu;
import android.view.MenuItem;

class	MenuAssistant
{

	public static void setCheckOnNthItemInGroupIDInMenu(int anIndex, int anId, Menu aMenu)
	{
		MenuItem		item;
		Menu			groupMenu;
		MenuItem		renderItem;

		item = aMenu.findItem(anId);
		if(item != null)
		{
			groupMenu = item.getSubMenu();
			if(groupMenu != null)
			{
				renderItem = groupMenu.getItem(anIndex);
				if(renderItem != null)
					renderItem.setChecked(true);
			}
		}
	}
}