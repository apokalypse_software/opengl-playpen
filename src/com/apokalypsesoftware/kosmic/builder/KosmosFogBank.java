/*
 * (C) Copyright 2012 Apokalypse Software Corp.
 *
 * Portions from Chris Aliotta's Cool Looking Fog lesson (#16) on
 * the NeHe OpenGL tutorials site.
 * http://nehe.gamedev.net/tutorial/cool_looking_fog/19001/
 * 
 * (C) Copyright Chris Aliotta
 * 	 
 * Portions Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apokalypsesoftware.kosmic.builder;

import static android.opengl.GLES10.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

class KosmosFogBank
{
	FloatBuffer			fogColor;
	int					fogModeIndex;
	float				defaultFogColor[] = { 1f, 1f, 1f, 1.0f};      // Fog Color

	KosmosFogBank()
	{
		setColor(defaultFogColor);
	}

	float[] getColor()
	{
		return	(fogColor.array());
	}

	void setColor(float aColor[])
	{
		ByteBuffer tbb = ByteBuffer.allocateDirect(aColor.length * 4);

		tbb.order(ByteOrder.nativeOrder());
		fogColor = tbb.asFloatBuffer();
		fogColor.put(aColor);
		fogColor.position(0);
	}

	float getMode()
	{
		return	(fogModeIndex);
	}

	void setMode(int aMode)
	{
		fogModeIndex = aMode;
	}

	void draw()
	{
		int	fogMode;
		int fogModes[]= { -1, GL_EXP, GL_EXP2, GL_LINEAR };   // Storage For Three Types Of Fog

		fogMode = fogModes[fogModeIndex];
		if(fogMode != -1)
		{
			glFogf(GL_FOG_MODE, fogModes[fogModeIndex]);        // Fog Mode
			glFogfv(GL_FOG_COLOR, fogColor);            // Set Fog Color
			glFogf(GL_FOG_DENSITY, 0.75f);              // How Dense Will The Fog Be
			glHint(GL_FOG_HINT, GL_DONT_CARE);          // Fog Hint Value
			glFogf(GL_FOG_START, 1.0f);             // Fog Start Depth
			glFogf(GL_FOG_END, 5.0f);               // Fog End Depth
			glEnable(GL_FOG);                   // Enables GL_FOG
		}
		else
			glDisable(GL_FOG);                   // Enables GL_FOG
	}
}